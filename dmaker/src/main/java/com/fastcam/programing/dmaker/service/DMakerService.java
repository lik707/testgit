package com.fastcam.programing.dmaker.service;

import com.fastcam.programing.dmaker.Exception.DMakerException;
import com.fastcam.programing.dmaker.code.StatusCode;
import com.fastcam.programing.dmaker.dto.CreateDeveloper;
import com.fastcam.programing.dmaker.dto.DeveloperDetailDto;
import com.fastcam.programing.dmaker.dto.DeveloperDto;
import com.fastcam.programing.dmaker.dto.EditDeveloper;
import com.fastcam.programing.dmaker.entity.Developer;
import com.fastcam.programing.dmaker.entity.RetiredDeveloper;
import com.fastcam.programing.dmaker.repository.DeveloperRepository;
import com.fastcam.programing.dmaker.repository.RetiredDeveloperRepository;
import com.fastcam.programing.dmaker.type.DeveloperLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static com.fastcam.programing.dmaker.Exception.DMakerErrorCode.*;

@Service
@RequiredArgsConstructor
public class DMakerService {
    private final DeveloperRepository developerRepository;
    private final RetiredDeveloperRepository retiredDeveloperRepository;

    public List<DeveloperDto> getAllEmployedDevelopers() {
        return developerRepository.findDeveloperByStatusCodeEquals(StatusCode.EMPLOYED)
                .stream()
                .map(DeveloperDto::fromEntity)
                .collect(Collectors.toList());

    }

    @Transactional
    public CreateDeveloper.Response createDeveloper(CreateDeveloper.Request request) {
        validateCreateDeveloperRequest(request);

        // business logic start

        Developer developer = Developer.builder()
                .developerLevel(request.getDeveloperLevel())
                .developerSkillType(request.getDeveloperSkillType())
                .experienceYears(request.getExperienceYears())
                .memberId(request.getMemberId())
                .statusCode(StatusCode.EMPLOYED)
                .name(request.getName())
                .age(request.getAge())
                .build();

        developerRepository.save(developer);

        return CreateDeveloper.Response.formEntity(developer);
        //business logic end
    }

    private void validateCreateDeveloperRequest(CreateDeveloper.Request request) {
        //business validation

        validateDeveloperLevel(request.getDeveloperLevel(), request.getExperienceYears());
//        Optional<Developer> developer = developerRepository.findByMemberId(request.getMemberId());
//                if(developer.isPresent())
//                    throw new DMakerException(DUPLICATED_MEMBER_ID);

        // java 11 기능 추가로 한 줄로 사용이 가능.
        developerRepository.findByMemberId(request.getMemberId())
                .ifPresent((develop -> {
                    throw new DMakerException(DUPLICATED_MEMBER_ID);
                }));
    }

    public DeveloperDetailDto getDeveloperDetail(String memberId) {
        return developerRepository.findByMemberId(memberId)
                .map(DeveloperDetailDto::fromEntity)
                .orElseThrow(() -> new DMakerException(NO_DEVELOPER));

    }

    @Transactional
    public void editDeveloper(String memberId, EditDeveloper.Request request) {
        validateEditDeveloperRequest(request, memberId);

        Developer developer = developerRepository.findByMemberId(memberId).orElseThrow(
                () -> new DMakerException(NO_DEVELOPER)
        );

        developer.updateDeveloper(request.getDeveloperLevel(), request.getDeveloperSkillType(), request.getExperienceYears());
    }


    private void validateEditDeveloperRequest(
            EditDeveloper.Request request,
            String memberId
    ) {

        validateDeveloperLevel(
                request.getDeveloperLevel(),
                request.getExperienceYears()
        );


    }

    private void validateDevelopLevel(EditDeveloper.Request request) {
        validateDeveloperLevel(request.getDeveloperLevel(), request.getExperienceYears());
    }

    private void validateDeveloperLevel(DeveloperLevel developerLevel, Integer experienceYears) {
        if (developerLevel == DeveloperLevel.SENIOR
                && experienceYears < 10) {
            throw new DMakerException(LEVEL_EXPERIENCE_YEARS_NOT_MATCHED);
        }
        if (developerLevel == DeveloperLevel.JUNIOR
                && experienceYears < 2 || experienceYears > 10) {
            throw new DMakerException(LEVEL_EXPERIENCE_YEARS_NOT_MATCHED);
        }
        if (developerLevel == DeveloperLevel.JUNGNIOR && experienceYears > 4) {
            throw new DMakerException(LEVEL_EXPERIENCE_YEARS_NOT_MATCHED);
        }
    }

    @Transactional
    public DeveloperDetailDto deleteDeveloper(String memberId) {
        //1. EMPLOYED -> RETIRED
        Developer developer = developerRepository.findByMemberId(memberId)
                .orElseThrow(() -> new DMakerException(NO_DEVELOPER));

        developer.setStatusCode(StatusCode.RETIRED);

        if (developer != null) throw new DMakerException(NO_DEVELOPER);

        //2. save into RetiredDeveloper
        RetiredDeveloper retiredDeveloper = RetiredDeveloper.builder()
                .memberId(memberId)
                .name(developer.getName())
                .build();
        retiredDeveloperRepository.save(retiredDeveloper);
        return DeveloperDetailDto.fromEntity(developer);
    }


}
