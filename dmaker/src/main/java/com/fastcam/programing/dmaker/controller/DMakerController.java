package com.fastcam.programing.dmaker.controller;

import com.fastcam.programing.dmaker.Exception.DMakerException;
import com.fastcam.programing.dmaker.dto.*;
import com.fastcam.programing.dmaker.service.DMakerService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 *
 */
@Slf4j
@RestController
@AllArgsConstructor
public class DMakerController {

    private final DMakerService dMakerService;

    @GetMapping("/developers")
    public List<DeveloperDto> getAllDevelopers() {

        // GET /developers Http/1.1
        log.info("GET /developers Http/1.1");

        return dMakerService.getAllEmployedDevelopers();
    }

    @GetMapping("/developer/{memberId}")
    public DeveloperDetailDto getDeveloperDetail(@PathVariable String memberId) {

        return dMakerService.getDeveloperDetail(memberId);
    }

    @PostMapping("/create-developer")
    public CreateDeveloper.Response createDeveloper(@Valid @RequestBody CreateDeveloper.Request request) {

        log.info("request : {}", request);

        return dMakerService.createDeveloper(request);
    }

    //PutMapping: 모든 정보를 다 수정
    //PathMapping: 해당 리소스 중에 특정한 리소스만 수정할 경우..

    @PutMapping("/developer/{memberId}")
    public void editDeveloper(@PathVariable String memberId, @RequestBody EditDeveloper.Request request) {
        dMakerService.editDeveloper(memberId, request);
    }

    @DeleteMapping("/developer/{memberId}")
    public void deleteDeveloper(@PathVariable String memberId) {
        dMakerService.deleteDeveloper(memberId);
    }

    @ResponseStatus(value = HttpStatus.CONFLICT)
    @ExceptionHandler(DMakerException.class)
    public DMakerErrorResponse handleException(DMakerException e, HttpServletRequest request) {
        log.error("errorCode: {}, url: {}, message: {}", e.getDMakerErrorCode(), request.getRequestURI(), e.getDetailMessage());
        return DMakerErrorResponse.builder()
                .errorCode(e.getDMakerErrorCode())
                .errorMessage(e.getDetailMessage())
                .build();
    }


}
